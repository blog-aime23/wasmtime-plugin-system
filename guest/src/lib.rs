
wit_bindgen::generate!({
    world: "hello-world",
    exports: {
        world: Host,
    },
});

struct Host {}

impl Guest for Host {
    fn run() {
        //We can call print here
        print("Test")
    }
}