use anyhow::Result;
use wasmtime::{
    component::{Component, Linker},
    AsContextMut, Config, Engine, Store,
};

wasmtime::component::bindgen!("hello-world");

struct HostImports {}

impl HelloWorldImports for HostImports {
    fn print(&mut self, msg: String) -> wasmtime::Result<()> {
        println!("{}", msg);
        Ok(())
    }
}

fn main() -> Result<()> {
    println!("Initializing...");

    let mut config = Config::default();
    config.wasm_component_model(true);
    
    let engine = Engine::new(&config)?;
    let component = Component::from_file(&engine, "plugins/plugin.wasm")?;
    let mut store = Store::new(&engine, HostImports {});
    let mut linker = Linker::new(&engine);

    HelloWorld::add_to_linker(&mut linker, |caller| {
        return caller;
    })?;

    let (hello_world, _) = HelloWorld::instantiate(store.as_context_mut(), &component, &linker)?;

    hello_world.call_run(store.as_context_mut())?;
    
    println!("Done.");
    Ok(())
}
